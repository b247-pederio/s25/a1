Activity:

1. In the s25 folder, create an activity folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Given the code provided by your instructor, fix the JSON string by looking for errors when the code tries to parse the JSON string.
4. Check and correct the JSON string if the following are missing:
Double quotes
Commas
Colons
Square/Curly brackets
Values
5. Stringify an object with the following properties:
Name (String)
Category (String)
Quantity (Number)
Model (String)
6. Create a git repository named s25.
7. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
8. Add the link in Boodle.